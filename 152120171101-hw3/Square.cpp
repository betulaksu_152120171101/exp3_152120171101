/*
 * Square.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Square.h"

Square::Square(double a) {
	setA(a);
	setB(a);
}

Square::~Square() {
}

void Square::setA(double a){
	this->a = a; 
}

void Square::setB(double b){
	this->b = a;
}

double Square::calculateCircumference(){
	return (a + b) * 2;
}

double Square::calculateArea(){
	return a * b;
}
